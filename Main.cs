﻿using GTA;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AircraftRadar
{
    public class AircraftRadar : Script
    {
        float radius;
        BlipSprite airplaneSprite, helicopterSprite;
        BlipColor blipColor;
        bool onlyShowAircraftWithDriver;

        List<Tuple<Vehicle, Blip>> vehsWithBlips = new List<Tuple<Vehicle, Blip>>();

        public AircraftRadar()
        {
            radius = Settings.GetValue("Settings", "Radius", 500f);
            airplaneSprite = Settings.GetValue("Settings", "Airplane_Blip_Sprite", BlipSprite.Plane);
            helicopterSprite = Settings.GetValue("Settings", "Helicopter_Blip_Sprite", BlipSprite.HelicopterAnimated);
            blipColor = Settings.GetValue("Settings", "Blip_Color", BlipColor.Blue);
            onlyShowAircraftWithDriver = Settings.GetValue("Settings", "Only_Show_Aircraft_Being_Flown", true);

            Interval = 500;
            Tick += AircraftRadar_Tick;
        }

        private bool isPlayerReady(Ped plrPed)
        {
            Vehicle plrVeh;
            Model plrVehModel;

            return plrPed != null && plrPed.Exists() && plrPed.IsAlive && plrPed.IsInVehicle() && (plrVeh = plrPed.CurrentVehicle) != null
                && plrVeh.Exists() && plrVeh.IsAlive && ((plrVehModel = plrVeh.Model).IsPlane || plrVehModel.IsHelicopter);
        }

        private void processBlips(Ped plrPed)
        {
            if (vehsWithBlips.Count != 0)
            {
                for (int i = 0; i < vehsWithBlips.Count; i++)
                {
                    Vehicle veh = vehsWithBlips[i].Item1;
                    Blip blip = vehsWithBlips[i].Item2;

                    if (veh == null || !veh.Exists() || !veh.IsAlive || (onlyShowAircraftWithDriver && veh.IsSeatFree(VehicleSeat.Driver)) || !isPlayerReady(plrPed) || veh.Position.DistanceTo(plrPed.Position) > radius)
                    {
                        if (blip != null && blip.Exists()) blip.Remove();

                        vehsWithBlips.RemoveAt(i);
                        i--;
                    }
                    else
                    {
                        blip.Rotation = (int)veh.Heading;
                    }

                    Yield();
                }
            }
        }

        private void AircraftRadar_Tick(object sender, EventArgs e)
        {
            Ped plrPed = Game.Player.Character;
            
            if (isPlayerReady(plrPed))
            {
                processBlips(plrPed);

                var nearbyVehicles = World.GetNearbyVehicles(plrPed, radius);

                if (nearbyVehicles != null && nearbyVehicles.Length != 0)
                {
                    foreach (Vehicle veh in nearbyVehicles)
                    {
                        Yield();

                        if (veh != null && veh.Exists() && veh.IsAlive && (!onlyShowAircraftWithDriver || !veh.IsSeatFree(VehicleSeat.Driver)) && !vehsWithBlips.Any(x => x.Item1 == veh))
                        {
                            var vehModel = veh.Model;

                            bool isPlane;

                            if (((isPlane = vehModel.IsPlane) || vehModel.IsHelicopter) && vehModel != "polmav")
                            {
                                Blip newBlip = veh.AddBlip();
                                newBlip.Sprite = isPlane ? airplaneSprite : helicopterSprite;
                                newBlip.Color = blipColor;
                                newBlip.Rotation = (int)veh.Heading;

                                vehsWithBlips.Add(new Tuple<Vehicle, Blip>(veh, newBlip));
                            }
                        }
                    }
                }
            }
            else processBlips(null);
        }
    }
}
